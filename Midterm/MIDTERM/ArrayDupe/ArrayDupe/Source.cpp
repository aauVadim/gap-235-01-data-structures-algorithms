#include <iostream>
#include <conio.h>
#include <map>

using std::cout;
using std::endl;

bool CheckForDuplicates(int* pArray, int size);
void FizzBuzz(int maxValue);
int FindIndex(const int* pArray, int size, int value);

const int k_arraySize = 5;

void main()
{
	int array[k_arraySize] = { 3, 2, 4, 2, 12 };



	//if (CheckForDuplicates(array, k_arraySize))
	//	cout << "Found duplicates" << endl;
	//else
	//	cout << "No duplicates" << endl; 

	//FizzBuzz(20);


	int input[] = { 2, 4, 6, 8 };
	int a = FindIndex(input, 4, 6);
	int b = FindIndex(input, 4, 9);
	cout << a << " " << b << endl;

	for (int i = 0; i < 4; ++i)
		cout << input[i] << " ";

	_getch();
}


int FindIndex(const int* pArray, int size, int value)
{
	bool found = false;

	int key = -1;

	for (int i = 0; i < size; ++i)
	{
		//Next one is bigger - Replace it with value
		if ((value < pArray[i + 1]) && !found)
		{
			key = i + 1;
			found = true;
		}

		//Edge case
		if (i == size - 1 && pArray[i - 1] < value)
			key = i + 1;

		//We found it
		if (pArray[i] == value)
			key = i;
	}

	return key;
}

bool CheckForDuplicates(int* pArray, int size)
{
	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < size; ++j)
		{
			if (pArray[i] == pArray[j])
				return true;
		}
	}

	return false;
}

void FizzBuzz(int maxValue)
{
	for (int i = 1; i < maxValue + 1; ++i)
	{
		if ((i % 3) == 0)
			cout << "Fizz" << endl;
		if ((i % 4) == 0)
			cout << "Buzz" << endl;
		if (((i % 3) == 0) && ((i % 4) == 0))
			cout << "FizzBuzz" << endl;
	}
}
