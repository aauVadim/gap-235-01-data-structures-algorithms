// Demo 6.1.cpp
#include <iostream>
#include <conio.h>
#include <assert.h>

//---------------------------------------------------------------------------------------------------------------------
// Homework!
// 
// Exercises:
// 10.1-1 - Draw a diagram
// 10.1-4 - See TODO's in Enqueue() and Dequeue()
// 10.2-2 - Do this as another section below
// 10.2-3 - Do this as another section below
// 
// Implement a InsertSorted():
//     - Assume the list is sorted, either InsertSorted() is used OR InsertFront(), NOT both.
//     - Make sure to handle edge cases!
// 
// Extra Credit:
// 10.1-2 - An explanation & diagram is good enough; no need to write code
// 10.1-6 - An explanation & diagram is good enough; no need to write code
// 10.1-7 - An explanation & diagram is good enough; no need to write code
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
//To Rez: 
//	All homework is done in this same document. So far: 
//		1. I have modified your LinkedList to be SinglyLinkedList
//		2. 10.1-4 - Is done in your implementation of Queue. 
//		3. 10.2-2/3 - I've used my SinglyLinkedList to do those assighments. LinkedListQueue::Enqueue() and LinkedListQueue::Dequeue()
//		   are using SingleLinkedList::GetListSize() for Overflow and Underflow control
//		4. Minuses: LinkedListQueue::Dequeue() - is not O(1). Currently it is O(n), I could not find a solution to that problem in given amout of time.  
//---------------------------------------------------------------------------------------------------------------------


using std::cout;

#ifdef _DEBUG
    #define ALLOW_UNIT_TESTS
#endif

//---------------------------------------------------------------------------------------------------------------------
// Stack
//---------------------------------------------------------------------------------------------------------------------
class Stack
{
    static const int k_stackSize = 15;

    int m_stack[k_stackSize];  // this data type should be whatever makes sense, most likely a template
    int m_top;  // this points to the top of the stack

public:
    Stack();

    void Push(int data);
    int Pop();

private:
    bool IsEmpty() const;
};


Stack::Stack()
    : m_top(-1)
{
    memset(m_stack, 0, sizeof(int) * k_stackSize);
}

bool Stack::IsEmpty() const
{
    assert(m_top >= -1);
    return (m_top <= -1);
}

void Stack::Push(int data)
{
    // check for stack overflow
    if (m_top + 1 >= k_stackSize)
    {
        cout << "Error: Stack overflow!\n";
        return;
    }

    // push the item onto the stack
    ++m_top;
    m_stack[m_top] = data;
}

int Stack::Pop()
{
    // check for stack underflow
    if (IsEmpty())
    {
        cout << "Error: Stack underflow!\n";
        return 0;
    }

    // pop the value and return it
    int value = m_stack[m_top];
    --m_top;
    return value;
}


//---------------------------------------------------------------------------------------------------------------------
// Queue
//---------------------------------------------------------------------------------------------------------------------
class Queue
{
    static const int k_queueSize = 15;

    int m_queue[k_queueSize];
    int m_head, m_tail;
	bool m_isEmpty;


public:
    Queue();

    void Enqueue(int data);
    int Dequeue();
	bool IsEmpty();
};

Queue::Queue()
    : m_head(0)
    , m_tail(0)
	, m_isEmpty(true)
{
    memset(m_queue, 0, sizeof(int) * k_queueSize);
}

void Queue::Enqueue(int data)
{
    // TODO: Add overflow error checking. Done

	//If queue is full we cannot accept an element
	if (!IsEmpty())
	{
		cout << "Queue overflow!; Exiting\n";
		return;
	}

	// queue the data
	m_queue[m_tail] = data;
	++m_tail;
    
	// wrap around if necessary
	if (m_tail >= k_queueSize)
        m_tail = 0;

	//if we are at our head - we are full. Cannot accept more elements. 
	if (m_tail == m_head)
		m_isEmpty = false;
}

int Queue::Dequeue()
{
    // TODO: Add underflow error checking. Done

	//If queue is empty we cannot pop an element.
	if (IsEmpty())
	{
		cout << "\n Queue underflow; Exiting\n";
		return -1;
	}

    // dequeue the data
    int value = m_queue[m_head];
    ++m_head;

    // wrap
    if (m_head >= k_queueSize)
        m_head = 0;

	//If head is at the tale - we are empty. We can accept more value
	if (m_head == m_tail)
		m_isEmpty = true;

    return value;
}
bool Queue::IsEmpty()
{
	return m_isEmpty;
}

//---------------------------------------------------------------------------------------------------------------------
// Linked List - With InsertFront()
//---------------------------------------------------------------------------------------------------------------------
class LinkedList
{
    struct Node
    {
        Node* m_pNext;
        Node* m_pPrev;
        int m_key;

        Node(int key) : m_pNext(nullptr), m_pPrev(nullptr), m_key(key) { }
    };

    Node* m_pHead;

public:
    LinkedList();

    // TODO: Write InsertSorted()

    void InsertFront(int key);  // insert key at the front of the list
    void Delete(int key);  // delete key from list
    void PrintList();  // print the entire list

private:
    Node* FindNode(int key);
};

LinkedList::LinkedList()
    : m_pHead(nullptr)
{
    //
}

void LinkedList::InsertFront(int key)
{
    // Create the new node
    Node* pNode = new Node(key);

    // Set the new node's next pointer to point to the current head of the list.  This might be nullptr.
    pNode->m_pNext = m_pHead;

    // If we already have a head node, it means the list isn't empty.  The old head node is now the second
    // node in the list.  Set it's previous pointer to the newly created node, which is the new head.
    if (m_pHead)
        m_pHead->m_pPrev = pNode;

    // Set the newly created node as the head.
    m_pHead = pNode;
}

void LinkedList::Delete(int key)
{
    Node* pNodeToDelete = FindNode(key);
    if (pNodeToDelete)
    {
        // If we have a valid previous node, point it to the next node.  If not, it means we're the head 
        // and we need to reset the m_pHead pointer.
        if (pNodeToDelete->m_pPrev)
            pNodeToDelete->m_pPrev->m_pNext = pNodeToDelete->m_pNext;
        else
            m_pHead = pNodeToDelete->m_pNext;

        // check the next pointer and rewire it to the previous node
        if (pNodeToDelete->m_pNext)
            pNodeToDelete->m_pNext->m_pPrev = pNodeToDelete->m_pPrev;

        // destroy the node
        delete pNodeToDelete;
    }
}

void LinkedList::PrintList()
{
    // special case for empty list
    if (!m_pHead)
    {
        cout << "<empty list>\n";
        return;
    }

    // walk through the list and pring out every node
    Node* pCurrent = m_pHead;
    while (pCurrent)
    {
        cout << pCurrent->m_key << " ";
        pCurrent = pCurrent->m_pNext;
    }

    cout << "\n";
}

LinkedList::Node* LinkedList::FindNode(int key)  // <3
{
    Node* pCurrent = m_pHead;
    while (pCurrent != nullptr && pCurrent->m_key != key)
    {
        pCurrent = pCurrent->m_pNext;
    }
    return pCurrent;  // this will either be a valid node or nullptr if it wasn't found
}


//---------------------------------------------------------------------------------------------------------------------
// Linked List - With InsertSorted()
//---------------------------------------------------------------------------------------------------------------------
class LinkedListInsertSorted
{
	struct Node
	{
		Node* m_pNext;
		Node* m_pPrev;
		int m_key;

		Node(int key) : m_pNext(nullptr), m_pPrev(nullptr), m_key(key) { }
	};

	Node* m_pHead;
	int m_listSize;

public:
	LinkedListInsertSorted();

	//TODO; Done;
	void InsertSorted(int key);
	void Delete(int key);  // delete key from list
	void PrintList();  // print the entire list
	int GetSize() { return m_listSize; }

private:
	Node* FindNode(int key);
};

LinkedListInsertSorted::LinkedListInsertSorted()
	: m_pHead(nullptr)
	, m_listSize(0)
{
	//
}
//Sorted list assumed
void LinkedListInsertSorted::InsertSorted(int key)
{
	//Create new node
	Node* pNewNode = new Node(key);
	
	//Starting at the head
	Node* pCurrentNode = m_pHead;

	//If list is not empty
	if (pCurrentNode)
	{
		//Walk trough the list. It's is O(n)
		for (int i = 0; i < m_listSize; ++i)
		{
			//if current key is smaller then new key. Inseert front
			if (pCurrentNode->m_key <= pNewNode->m_key)
			{
				//Storing pointer to next node
				pNewNode->m_pNext = pCurrentNode;
				//Storing pointer on next node to previous node;
				pNewNode->m_pNext->m_pPrev = pNewNode;
				//Assign new node to current node
				pCurrentNode = pNewNode;
				break;
			}
			else
				pCurrentNode = pCurrentNode->m_pNext;
		}
	}
	else
	{
		//Storing pointer to next node
		pNewNode->m_pNext = pCurrentNode;
		//Assign new node to current node
		pCurrentNode = pNewNode;
	}

	//Adding one to the list
	++m_listSize;
	//Assingning current node to head
	m_pHead = pCurrentNode;
}

void LinkedListInsertSorted::Delete(int key)
{
	Node* pNodeToDelete = FindNode(key);
	if (pNodeToDelete)
	{
		// If we have a valid previous node, point it to the next node.  If not, it means we're the head 
		// and we need to reset the m_pHead pointer.
		if (pNodeToDelete->m_pPrev)
			pNodeToDelete->m_pPrev->m_pNext = pNodeToDelete->m_pNext;
		else
			m_pHead = pNodeToDelete->m_pNext;

		// check the next pointer and rewire it to the previous node
		if (pNodeToDelete->m_pNext)
			pNodeToDelete->m_pNext->m_pPrev = pNodeToDelete->m_pPrev;

		//Minusing one from the size
		--m_listSize;
		
		// destroy the node
		delete pNodeToDelete;
	}
}

void LinkedListInsertSorted::PrintList()
{
	// special case for empty list
	if (!m_pHead)
	{
		cout << "<empty list>\n";
		return;
	}

	// walk through the list and pring out every node
	Node* pCurrent = m_pHead;
	while (pCurrent)
	{
		cout << pCurrent->m_key << " ";
		pCurrent = pCurrent->m_pNext;
	}
	cout << "List size: " << m_listSize << '\n';
	cout << "\n";
}

LinkedListInsertSorted::Node* LinkedListInsertSorted::FindNode(int key)  // <3
{
	Node* pCurrent = m_pHead;
	while (pCurrent != nullptr && pCurrent->m_key != key)
	{
		pCurrent = pCurrent->m_pNext;
	}
	return pCurrent;  // this will either be a valid node or nullptr if it wasn't found
}


//---------------------------------------------------------------------------------------------------------------------
// Single Linked List -  Used for 10.2-2 and 10.2-3
//---------------------------------------------------------------------------------------------------------------------
class SingleLinkedList
{
	struct Node
	{
		Node* m_pNext;
		int m_key;

		Node(int key) : m_pNext(nullptr), m_key(key) { }
	};

	Node* m_pHead;
	int m_listSize;

public:
	SingleLinkedList();

	// TODO: Write InsertSorted()

	void InsertFront(int key);  // insert key at the front of the list
	void Delete(int key);  // delete key from list
	void PrintList();  // print the entire list

	int GetListSize() { return m_listSize; };
	//Added this function to be able to pop element from fron or back of a list
	int Pop(bool isFront);

private:
	Node* FindNode(int key);
};

SingleLinkedList::SingleLinkedList()
	: m_pHead(nullptr)
	, m_listSize(0)
{
	//
}

void SingleLinkedList::InsertFront(int key)
{
	//Increasing list size
	++m_listSize;

	// Create the new node
	Node* pNode = new Node(key);

	// Set the new node's next pointer to point to the current head of the list.  This might be nullptr.
	pNode->m_pNext = m_pHead;

	// Set the newly created node as the head.
	m_pHead = pNode;
}

void SingleLinkedList::Delete(int key)
{
	Node* pNodeToDelete = FindNode(key);
	if (pNodeToDelete)
	{
		//Start at the head
		Node* pCheckNode = m_pHead;
		//Node is at the head
		if (pNodeToDelete == m_pHead)
		{
			m_pHead = pNodeToDelete->m_pNext;
		}
		//Else go from front
		else
		{
			while (pCheckNode)
			{
				if (pCheckNode->m_pNext == pNodeToDelete)
					pCheckNode->m_pNext = pNodeToDelete->m_pNext;

				//Incrementing
				pCheckNode = pCheckNode->m_pNext;
			}
		}

		--m_listSize;
		// destroy the node
		delete pNodeToDelete;
	}
}

void SingleLinkedList::PrintList()
{
	// special case for empty list
	if (!m_pHead)
	{
		cout << "<empty list>\n";
		return;
	}

	// walk through the list and pring out every node
	Node* pCurrent = m_pHead;
	while (pCurrent)
	{
		cout << pCurrent->m_key << " ";
		pCurrent = pCurrent->m_pNext;
	}

	cout << "\n";
}
//function used to ether pop elemnent from front of an array or from back of it. 
int SingleLinkedList::Pop(bool isFront)
{
	//Remove head
	if (isFront)
	{
		//take value from the head node
		int value = m_pHead->m_key;
		//Delete its node
		Delete(value);
		//return the valuse
		return value;
	}
	//Remove back
	else
	{
		Node* pNodeToPop = m_pHead;

		for (int i = 0; i < m_listSize; ++i)
		{
			if (i == m_listSize - 1)
			{
				//take value from the back node
				int value = pNodeToPop->m_key;
				//Delete its node
				Delete(value);
				//return the valuse
				return value;
			}
			else
			{
				pNodeToPop = pNodeToPop->m_pNext;
			}
		}

		//something went wrong
		return -1;
	}

	//Something went bad
	return -1;
}

SingleLinkedList::Node* SingleLinkedList::FindNode(int key)  // <3
{
	Node* pCurrent = m_pHead;
	while (pCurrent != nullptr && pCurrent->m_key != key)
	{
		pCurrent = pCurrent->m_pNext;
	}
	return pCurrent;  // this will either be a valid node or nullptr if it wasn't found
}




//---------------------------------------------------------------------------------------------------------------------
// Queue with Single Linked List - Assighment: 10-2.3
//---------------------------------------------------------------------------------------------------------------------
class LinkedListQueue
{
	static const int k_queueSize = 15;

	SingleLinkedList m_singleLinkedList;
	int m_head, m_tail;

public:
	LinkedListQueue();

	void Enqueue(int data);
	int Dequeue();
};

LinkedListQueue::LinkedListQueue()
	: m_head(0)
	, m_tail(0)
{

}

void LinkedListQueue::Enqueue(int data)
{
	// TODO: Add overflow error checking. Done
	if (m_singleLinkedList.GetListSize() < k_queueSize)
	{
		// queue the data
		m_singleLinkedList.InsertFront(data);
		++m_tail;

		// wrap around if necessary
		if (m_tail >= k_queueSize)
			m_tail = 0;
	}
	else
	{
		cout << "Linked List Queue overflow!; Exiting.\n";
		return;
	}
}

int LinkedListQueue::Dequeue()
{
	// TODO: Add underflow error checking. Done
	if (m_singleLinkedList.GetListSize() > 0)
	{
		int value = m_singleLinkedList.Pop(false);
		++m_head;
		// wrap
		if (m_head >= k_queueSize)
			m_head = 0;

		return value;
	}
	else
	{
		cout << "Linked List Queue Underflow!; Exiting.\n";
		return -1;
	}
}
//---------------------------------------------------------------------------------------------------------------------
// Stack with Single Linked List - Assighment: 10-2.2
//---------------------------------------------------------------------------------------------------------------------
class SingleLinkedListStack
{
	static const int k_stackSize = 15;

	//int m_stack[k_stackSize];  // this data type should be whatever makes sense, most likely a template
	SingleLinkedList m_singleLinkedList;
	int m_top;  // this points to the top of the stack

public:
	SingleLinkedListStack();

	void Push(int data);
	int Pop();

private:
	bool IsEmpty() const;
};


SingleLinkedListStack::SingleLinkedListStack()
	: m_top(-1)
{

}

bool SingleLinkedListStack::IsEmpty() const
{
	assert(m_top >= -1);
	return (m_top <= -1);
}

void SingleLinkedListStack::Push(int data)
{
	// check for stack overflow
	if (m_top + 1 >= k_stackSize)
	{
		cout << "Error: Stack overflow!\n";
		return;
	}

	// push the item onto the stack
	++m_top;
	//Insert it into list
	m_singleLinkedList.InsertFront(data);
}

int SingleLinkedListStack::Pop()
{
	// check for stack underflow
	if (IsEmpty())
	{
		cout << "Error: Stack underflow!\n";
		return 0;
	}

	// pop the value and return it
	int value = m_singleLinkedList.Pop(true);
	--m_top;
	return value;
}

//---------------------------------------------------------------------------------------------------------------------
// Test functions
//---------------------------------------------------------------------------------------------------------------------
void TestStack()
{
#ifdef ALLOW_UNIT_TESTS
    cout << "Stack: \n";

    Stack stack;

    stack.Push(2);
    stack.Push(4);
    stack.Push(6);

    cout << stack.Pop() << " ";
    cout << stack.Pop() << " ";
    cout << stack.Pop() << " ";

    cout << "\n\n";
#endif
}

void TestQueue()
{
    cout << "Queue: \n";

    Queue queue;
	int testInserts = 17;
    //queue.Enqueue(2);
    //queue.Enqueue(4);
    //queue.Enqueue(6);

	for (int i = 0; i < 16; ++i)
		queue.Enqueue(i);

	for (int i = 0; i < 20; ++i)
		cout << queue.Dequeue() << " ";

    cout << "\n\n";
}

void TestLinkedList()
{
    LinkedList list;

    list.InsertFront(2);
    list.InsertFront(4);
    list.InsertFront(6);
    list.InsertFront(8);

    list.PrintList();  // 8, 6, 4, 2

    // test middle deletion
    list.Delete(4);
    list.PrintList();

    // test front of list
    list.Delete(8);
    list.PrintList();

    // test back of list
    list.Delete(2);
    list.PrintList();

    // test final element
    list.Delete(6);
    list.PrintList();

    // test no element
    list.Delete(5);
    list.PrintList();
}

void TestSingleLinkedList()
{
	SingleLinkedList singlelinkedList; 

	cout << "Single Linked List:\n";

	singlelinkedList.InsertFront(2);
	singlelinkedList.InsertFront(4);
	singlelinkedList.InsertFront(6);
	singlelinkedList.InsertFront(8);

	singlelinkedList.PrintList();  // 8, 6, 4, 2

	// test middle deletion
	singlelinkedList.Delete(4);
	singlelinkedList.PrintList();

	// test front of list
	singlelinkedList.Delete(8);
	singlelinkedList.PrintList();

	// test back of list
	singlelinkedList.Delete(2);
	singlelinkedList.PrintList();

	// test final element
	singlelinkedList.Delete(6);
	singlelinkedList.PrintList();

	// test no element
	singlelinkedList.Delete(5);
	singlelinkedList.PrintList();

}

void TestSingleLinkedListQueue()
{
	cout << "Single Linked List - Queue: \n";

	LinkedListQueue queue;

	int testInserts = 5;

	for (int i = 0; i < testInserts; ++i)
		queue.Enqueue(i);

	for (int i = 0; i < testInserts; ++i)
		cout << queue.Dequeue() << " ";


	cout << "\n\n";
}
void TestSinglelinkedListStack()
{
	cout << "Single Linked List - Stack: \n";

	SingleLinkedListStack queue;

	int testInserts = 5;

	for (int i = 0; i < testInserts; ++i)
		queue.Push(i);

	for (int i = 0; i < testInserts; ++i)
		cout << queue.Pop() << " ";


	cout << "\n\n";
}

void TestDLinkedListInstertSorted()
{
	LinkedListInsertSorted linkedListInsertSorted;

	cout << "Double Linkled List with InsertSorted: \n";

	linkedListInsertSorted.InsertSorted(2);
	linkedListInsertSorted.InsertSorted(4);
	linkedListInsertSorted.InsertSorted(6);
	linkedListInsertSorted.InsertSorted(8);

	linkedListInsertSorted.PrintList();  // 8, 6, 4, 2

	// test middle deletion
	linkedListInsertSorted.Delete(4);
	linkedListInsertSorted.PrintList();

	// test front of list
	linkedListInsertSorted.Delete(8);
	linkedListInsertSorted.PrintList();

	// test back of list
	linkedListInsertSorted.Delete(2);
	linkedListInsertSorted.PrintList();

	// test final element
	linkedListInsertSorted.Delete(6);
	linkedListInsertSorted.PrintList();

	// test no element
	linkedListInsertSorted.Delete(5);
	linkedListInsertSorted.PrintList();
}
//---------------------------------------------------------------------------------------------------------------------
// Main function
//---------------------------------------------------------------------------------------------------------------------
void main()
{
    //TestStack();
    //TestQueue();
    //TestLinkedList();
	TestSingleLinkedList();
	TestSingleLinkedListQueue(); 
	TestSinglelinkedListStack();
	
	TestDLinkedListInstertSorted();

    _getch();
}
