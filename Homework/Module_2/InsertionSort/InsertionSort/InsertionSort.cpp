// InsertionSort.cpp
// Name: Vadiom Osipov 

#include <iostream>
#include <conio.h>

using namespace std;

const int k_arraySize = 5;

void PrintArray(int* pArray, int size);
//[rez]
void InsertionSort(int* pArray, int size);
//[vo]
void InsertionSortBigNumbers(int* pArray, int size);



void main()
{
	//[rez]
	int dataArray[k_arraySize] = { 15, 42, 1, 8, -4 };
	//[vo]
	int dataArrayBigNumbers[k_arraySize] = {42, 6, 2, 16, 75};

	PrintArray(dataArray, k_arraySize);
	InsertionSort(dataArray, k_arraySize);
	PrintArray(dataArray, k_arraySize);
	
	_getch();

	PrintArray(dataArrayBigNumbers, k_arraySize);
	InsertionSortBigNumbers(dataArrayBigNumbers, k_arraySize);
	PrintArray(dataArrayBigNumbers, k_arraySize);

	_getch();
}
//[rez]
void InsertionSort(int* pArray, int size)	//Analisys - 
{
	//Best Case:	O(n) - linear
	//Worst Case:	O(n^2) 

	for (int j = 1;							//C1  
		j < size;							//C2 * (n-1)
		++j)								//C3 * (n-1)
	{
		//Key - storing variable
		int key = pArray[j];				//C4 * (n - 1)
		//Stepping back
		int i = j - 1;						//C5 * (n - 1)
		while (i >= 0 && pArray[i] > key)	//SUM(C6 * (m - 1)) <2 - n> 
		{
			pArray[i + 1] = pArray[i];
			--i;
		}
		pArray[i + 1] = key;
	}
}
//[vo]
void InsertionSortBigNumbers(int* pArray, int size)
{
	for (int j = 1; j < size; ++j)
	{
		//Key - storing variable
		int key = pArray[j];				
		//Stepping back
		int i = j - 1;						
		while (i >= 0 && pArray[i] < key)	//Checking bigger
		{
			pArray[i + 1] = pArray[i];
			--i;
		}
		pArray[i + 1] = key;
	}
}
//[rez]
void PrintArray(int* pArray, int size)
{
	cout << "{"; 
	for (int i = 0; i < size; ++i)
	{
		cout << pArray[i] << ", "; 
	}
	cout << "}\n";
}