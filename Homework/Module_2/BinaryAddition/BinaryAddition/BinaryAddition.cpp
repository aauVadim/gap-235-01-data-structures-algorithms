// BinbaryAddition.cpp
// Name: Vadim Osipov
// Date: 9/21

#include <iostream>
#include <conio.h>

void PrintArrays(int* aA, int* aB, int* result);
void AddArrays(int* aA, int* aB, int* result);

const int g_kLenght = 8;

void main()
{
	//Candidate arrays
	int arrayA[g_kLenght] = { 0, 1, 1, 0, 1, 0, 1, 0 };
	int arrayB[g_kLenght] = { 1, 0, 1, 1, 0, 0, 1, 1 };
	//Neat trick learned from Josh
	int resultArrayC[g_kLenght + 1] = { 0 };
	//Printing
	PrintArrays(arrayA, arrayB, resultArrayC);
	//Adding
	AddArrays(arrayA, arrayB, resultArrayC);
	//Input
	_getch();
	//Reprint added arrays 
	PrintArrays(arrayA, arrayB, resultArrayC);
	//Input
	_getch();
}

void PrintArrays(int* aA, int* aB, int* result)
{
	std::cout << "Array A: \t{"; 
	//Printing Array A
	for (int i = 0; i < g_kLenght; ++i)
	{
		if (i == 4)
			std::cout << "\t\t";
		
		std::cout << aA[i] << ", ";
	}
	std::cout << "}\n";

	std::cout << "Array B: \t{";
	//Printing Array B
	for (int i = 0; i < g_kLenght; ++i)
	{
		if (i == 4)
			std::cout << "\t\t";

		std::cout << aB[i] << ", ";
	}
	std::cout << "}\n";

	std::cout << "Result Array: \t{";
	//Printing Result Array + 1 to lenght 
	for (int i = 0; i < g_kLenght + 1; ++i)
	{
		if (i == 5)
			std::cout << "\t";
		std::cout << result[i] << ", ";
	}
	std::cout << "}\n";
}

void AddArrays(int* aA, int* aB, int* result)
{
	int sum = 0; 
	int remeinder = 0;
	//Starting in the back of array
	for (int i = g_kLenght; i > 0; --i)
	{
		//Calculating Summ to compare it down the road
		sum = aA[i - 1] + aB[i - 1] + remeinder;
		switch (sum)	//Checking
		{
		case 0:
			sum = 0;
			remeinder = 0;
			break;
		case 1:
			sum = 1;
			remeinder = 0;
			break;
		case 2:
			sum = 0;
			remeinder = 1;
			break;
		case 3:
			sum = 1;
			remeinder = 1;
			break;
		}
		//Assigning sum to result array
		result[i] = sum;
		//If we are at the end of array, save reminder to 0 element
		if (i == 1)
		{
			result[0] = remeinder;
		}
	}
}