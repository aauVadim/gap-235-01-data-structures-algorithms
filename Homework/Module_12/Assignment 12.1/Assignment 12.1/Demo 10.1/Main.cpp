// Main.cpp
#include <iostream>
#include <conio.h>
#include <assert.h>
#include <string>

using namespace std;

//---------------------------------------------------------------------------------------------------------------------
// Binary Search Tree Declaration
//---------------------------------------------------------------------------------------------------------------------
template <class KeyType, class DataType>
class BinarySearchTree
{
//public:
//    typedef _KeyType KeyType;
//    typedef _DataType DataType;

    enum class NodeColor : unsigned char  // now this variable is one byte
    {
        k_red,
        k_black,
    };

private:
    struct Node
    {
        Node* m_pParent;
        Node* m_pLeft;
        Node* m_pRight;

        KeyType m_key;
        DataType m_data;

        NodeColor m_color;

        Node(const KeyType& key, const DataType& data)
            : m_pLeft(nullptr)
            , m_pRight(nullptr)
            , m_pParent(nullptr)
            , m_key(key)
            , m_data(data)
            , m_color(NodeColor::k_red)
        {
            //
        }

        ~Node()
        {
            delete m_pLeft;
            delete m_pRight;

            m_pParent = nullptr;
            m_pLeft = nullptr;
            m_pRight = nullptr;
        }

        // used so we can surgically remove a node
        void ClearPointers()
        {
            m_pParent = nullptr;
            m_pLeft = nullptr;
            m_pRight = nullptr;
        }

        void PrintNode(int level) const
        {
            // print out spaces for each level
            for (int i = 0; i < level; ++i)
            {
                std::cout << "  ";
            }

            // print out this node
            std::cout << m_key << " -> " << m_data << ((m_color == NodeColor::k_black) ? (" (B)") : (" (R)")) << std::endl;

            // print the left node
            if (m_pLeft)
                m_pLeft->PrintNode(level + 1);

            // print the right node
            if (m_pRight)
                m_pRight->PrintNode(level + 1);
        }
    };

    Node* m_pRoot;

public:
    BinarySearchTree();
    ~BinarySearchTree();

    void Insert(const KeyType& key, const DataType& data);
    void Delete(const KeyType& key);
	void DeleteRBNode(const KeyType& key);

    bool FindData(const KeyType& key, DataType& outData) const;
    bool FindMinimum(DataType& outData) const;
    bool FindMaximum(DataType& outData) const;

    void PrintNodesInOrder() const;
    void PrintTree() const;

private:
    Node* FindNode(const KeyType& key) const;
    Node* FindMinimumNode(Node* pRoot) const;
    Node* FindMaximumNode(Node* pRoot) const;
    void InternalPrintNodesInOrder(Node* pNode) const;
    void Transplant(Node* pNodeToReplace, Node* pReplacingNode);

    // red/black stuff
    void FixupAfterInsert(Node* pNodeInserted);

    // Node rotation.  In this example, pNode == x.
    // 
    //     y  <-- RL  x
    //    /   RR -->   \
    //   x              y
    void RotateLeft(Node* pNode);
    void RotateRight(Node* pNode);

    NodeColor GetNodeColor(Node* pNode) const;
	//-----------------------------------------------------------------------------------------------------------------
	//	Assighment 12.1
	//-----------------------------------------------------------------------------------------------------------------
	void DeleteRBNodeInternal(Node* pNodeToDelete);
	void DeleteRBNodeInternalFixup(Node* pDeletedNode);
};


//---------------------------------------------------------------------------------------------------------------------
// Binary Search Tree Definition
//---------------------------------------------------------------------------------------------------------------------
template <class KeyType, class DataType>
BinarySearchTree<KeyType, DataType>::BinarySearchTree()
    : m_pRoot(nullptr)
{
    //
}

template <class KeyType, class DataType>
BinarySearchTree<KeyType, DataType>::~BinarySearchTree()
{
    delete m_pRoot;
}

template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::Insert(const KeyType& key, const DataType& data)
{
    // create the new node
    Node* pNewNode = new Node(key, data);

    Node* pParent = nullptr;  // parent of the current node
    Node* pCurrent = m_pRoot;  // the current node

    // walk through the tree to find the spot to insert this node
    while (pCurrent)
    {
        // set last node, aka parent node
        pParent = pCurrent;

        // mode to the next branch
        if (key <= pCurrent->m_key)
            pCurrent = pCurrent->m_pLeft;
        else
            pCurrent = pCurrent->m_pRight;
    }

    // Once we get here, pParent will be the root of where we need to insert the new node.  If it's nullptr, it
    // means that the tree is empty, so we put it at the root.
    if (pParent == nullptr)
    {
        m_pRoot = pNewNode;
    }
    else if (key <= pParent->m_key)
    {
        pParent->m_pLeft = pNewNode;
        pNewNode->m_pParent = pParent;
    }
    else
    {
        pParent->m_pRight = pNewNode;
        pNewNode->m_pParent = pParent;
    }

    FixupAfterInsert(pNewNode);
}

//-----------------------------------------------------------------------------------------------------------------
//	Assighment 12.1
//-----------------------------------------------------------------------------------------------------------------
// External function to call
template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::DeleteRBNode(const KeyType& key)
{
	DeleteRBNodeInternal(FindNode(key));
}

template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::DeleteRBNodeInternal(Node* pNodeToDelete)
{
	Node* pY = pNodeToDelete;
	Node* pX = nullptr;
	NodeColor pCurrentColor = GetNodeColor(pY);
	
	if (pNodeToDelete->m_pLeft == nullptr)
	{
		pX = pNodeToDelete->m_pRight;
		Transplant(pNodeToDelete, pNodeToDelete->m_pRight);
	}
	else if (pNodeToDelete->m_pRight == nullptr)
	{
		pX = pNodeToDelete->m_pLeft;
		Transplant(pNodeToDelete, pNodeToDelete->m_pLeft);
	}
	else
	{
		pY = FindMinimumNode(pNodeToDelete->m_pRight);
		pCurrentColor = GetNodeColor(pY);

		pX = pY->m_pRight;

		if (pY->m_pParent == pNodeToDelete)
		{
			pX->m_pParent = pY;
		}
		else
		{
			Transplant(pY, pY->m_pRight);
			pY->m_pRight = pNodeToDelete->m_pRight;
			pY->m_pRight->m_pParent = pY;
		}

		Transplant(pNodeToDelete, pY);
		pY->m_pLeft = pNodeToDelete->m_pLeft;
		pY->m_pLeft->m_pParent = pY;
		pY->m_color = pNodeToDelete->m_color;
	}

	if (pCurrentColor == NodeColor::k_black)
	{
		DeleteRBNodeInternalFixup(pX);
	}
}

template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::DeleteRBNodeInternalFixup(Node* pDeletedNode)
{
	while (pDeletedNode != m_pRoot && pDeletedNode->m_color == NodeColor::k_black)
	{
		if (pDeletedNode == pDeletedNode->m_pParent->m_pLeft)
		{
			Node* pTemp = pDeletedNode->m_pParent->m_pRight;
			if (pTemp->m_color == NodeColor::k_red)
			{
				pTemp->m_color = NodeColor::k_black;
				pDeletedNode->m_pParent->m_color = NodeColor::k_red;

				RotateLeft(pDeletedNode->m_pParent);
				pTemp = pDeletedNode->m_pParent->m_pRight;
			}
			if (pTemp->m_pLeft->m_color == NodeColor::k_black && pTemp->m_pRight->m_color == NodeColor::k_black)
			{
				pTemp->m_color = NodeColor::k_red;
				pDeletedNode = pDeletedNode->m_pParent;
			}
			else if (pTemp->m_pRight->m_color == NodeColor::k_black)
			{
				pTemp->m_pLeft->m_color = NodeColor::k_black;
				pTemp->m_color = NodeColor::k_red;
				RotateRight(pTemp);
				pTemp = pDeletedNode->m_pParent->m_pRight; 
			}

			pTemp->m_color = pDeletedNode->m_pParent->m_color;
			pDeletedNode->m_pParent->m_color = NodeColor::k_black;
			pTemp->m_pRight->m_color = NodeColor::k_black;
			RotateLeft(pDeletedNode->m_pParent);
			pDeletedNode = m_pRoot;
		}
		else 
		{
			Node* pTemp = pDeletedNode->m_pParent->m_pLeft;
			if (pTemp->m_color == NodeColor::k_red)
			{
				pTemp->m_color = NodeColor::k_black;
				pDeletedNode->m_pParent->m_color = NodeColor::k_red;

				RotateRight(pDeletedNode->m_pParent);
				pTemp = pDeletedNode->m_pParent->m_pLeft;
			}
			if (pTemp->m_pRight->m_color == NodeColor::k_black && pTemp->m_pLeft->m_color == NodeColor::k_black)
			{
				pTemp->m_color = NodeColor::k_red;
				pDeletedNode = pDeletedNode->m_pParent;
			}
			else if (pTemp->m_pLeft->m_color == NodeColor::k_black)
			{
				pTemp->m_pRight->m_color = NodeColor::k_black;
				pTemp->m_color = NodeColor::k_red;
				RotateLeft(pTemp);
				pTemp = pDeletedNode->m_pParent->m_pLeft;
			}

			pTemp->m_color = pDeletedNode->m_pParent->m_color;
			pDeletedNode->m_pParent->m_color = NodeColor::k_black;
			pTemp->m_pLeft->m_color = NodeColor::k_black;
			RotateRight(pDeletedNode->m_pParent);
			pDeletedNode = m_pRoot;
		}

		pDeletedNode->m_color = NodeColor::k_black;
	}
}


template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::Delete(const KeyType& key)
{
    // find the node that we want to delete
    Node* pNodeToDelete = FindNode(key);
    if (!pNodeToDelete)
        return;

    // Case 1: Node has no left children.  It's a simple matter of shifting the right node up.
    if (pNodeToDelete->m_pLeft == nullptr)
    {
        Transplant(pNodeToDelete, pNodeToDelete->m_pRight);
    }

    // Case 2: Node has no right children.  Same thing; shift left node up.
    else if (pNodeToDelete->m_pRight == nullptr)
    {
        Transplant(pNodeToDelete, pNodeToDelete->m_pLeft);
    }

    // Case 3: Both children are valid.  :(
    else
    {
        // The successor is guaranteed to be the node with the smallest value in our node's right branch.  We need to 
        // surgically remove the successor and replace our node with it.
        Node* pSuccessor = FindMinimumNode(pNodeToDelete->m_pRight);
        assert(pSuccessor);  // this should be impossible

        // If the successor is not the direct child of the node we want to delete, we need to replace this successor 
        // with its right child.
        if (pSuccessor->m_pParent != pNodeToDelete)
        {
            Transplant(pSuccessor, pSuccessor->m_pRight);
            pSuccessor->m_pRight = pNodeToDelete->m_pRight;
            pSuccessor->m_pRight->m_pParent = pSuccessor;
        }

        Transplant(pNodeToDelete, pSuccessor);
        pSuccessor->m_pLeft = pNodeToDelete->m_pLeft;
        pSuccessor->m_pLeft->m_pParent = pSuccessor;
    }

    // destroy node
    pNodeToDelete->ClearPointers();
    delete pNodeToDelete;
}

template <class KeyType, class DataType>
bool BinarySearchTree<KeyType, DataType>::FindData(const KeyType& key, DataType& outData) const
{
    Node* pNode = FindNode(key);
    if (pNode)
    {
        outData = pNode->m_data;
        return true;
    }
    return false;
}

template <class KeyType, class DataType>
bool BinarySearchTree<KeyType, DataType>::FindMinimum(DataType& outData) const
{
    Node* pMinNode = FindMinimumNode(m_pRoot);
    if (pMinNode)
    {
        outData = pMinNode->m_data;
        return true;
    }

    return false;
}

template <class KeyType, class DataType>
bool BinarySearchTree<KeyType, DataType>::FindMaximum(DataType& outData) const
{
    Node* pMaxNode = FindMaximumNode(m_pRoot);
    if (pMaxNode)
    {
        outData = pMaxNode->m_data;
        return true;
    }

    return false;
}

template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::PrintNodesInOrder() const
{
    InternalPrintNodesInOrder(m_pRoot);
}

template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::PrintTree() const
{
    if (m_pRoot)
        m_pRoot->PrintNode(0);
}

template <class KeyType, class DataType>
typename BinarySearchTree<KeyType, DataType>::Node* BinarySearchTree<KeyType, DataType>::FindNode(const KeyType& key) const
{
    Node* pCurrent = m_pRoot;
    while (pCurrent)
    {
        // if this is the node we're looking for, return it
        if (key == pCurrent->m_key)
            return pCurrent;

        // move to the next branch
        if (key <= pCurrent->m_key)
            pCurrent = pCurrent->m_pLeft;
        else
            pCurrent = pCurrent->m_pRight;
    }

    // if we get here, we didn't find the node
    return nullptr;
}

template <class KeyType, class DataType>
typename BinarySearchTree<KeyType, DataType>::Node* BinarySearchTree<KeyType, DataType>::FindMinimumNode(Node* pRoot) const
{
    if (!pRoot)
        return nullptr;

    Node* pCurrent = pRoot;
    while (pCurrent->m_pLeft)
    {
        pCurrent = pCurrent->m_pLeft;
    }

    return pCurrent;
}

template <class KeyType, class DataType>
typename BinarySearchTree<KeyType, DataType>::Node* BinarySearchTree<KeyType, DataType>::FindMaximumNode(Node* pRoot) const
{
    if (!pRoot)
        return nullptr;

    Node* pCurrent = pRoot;
    while (pCurrent->m_pRight)
    {
        pCurrent = pCurrent->m_pRight;
    }

    return pCurrent;
}

template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::InternalPrintNodesInOrder(Node* pNode) const
{
    if (pNode)
    {
        InternalPrintNodesInOrder(pNode->m_pLeft);
        std::cout << pNode->m_key << " => " << pNode->m_data << " (" << (GetNodeColor(pNode) == NodeColor::k_red ? "red" : "black") << ")\n";
        InternalPrintNodesInOrder(pNode->m_pRight);
    }
}

template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::Transplant(Node* pNodeToReplace, Node* pReplacingNode)
{
    // check to see if we're replacing the root node
    if (!pNodeToReplace->m_pParent)
        m_pRoot = pReplacingNode;

    // If we get here, we're either a right or a left child node.  Handle each case by pointing those nodes to 
    // the replacing node.
    else if (pNodeToReplace == pNodeToReplace->m_pParent->m_pLeft)
        pNodeToReplace->m_pParent->m_pLeft = pReplacingNode;
    else
        pNodeToReplace->m_pParent->m_pRight = pReplacingNode;

    // update the parent of the replacing node, assuming the replacing node is valid
    if (pReplacingNode)
        pReplacingNode->m_pParent = pNodeToReplace->m_pParent;
}

template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::FixupAfterInsert(Node* pNodeInserted)
{
    while (pNodeInserted != m_pRoot && GetNodeColor(pNodeInserted->m_pParent) == NodeColor::k_red)
    {
        // if our parent is the left child
        if (pNodeInserted->m_pParent == pNodeInserted->m_pParent->m_pParent->m_pLeft)
        {
            // get the uncle
            Node* pUncle = pNodeInserted->m_pParent->m_pParent->m_pRight;

            // Case 1: Node's uncle is red
            if (GetNodeColor(pUncle) == NodeColor::k_red)
            {
                pNodeInserted->m_pParent->m_color = NodeColor::k_black;
                pUncle->m_color = NodeColor::k_black;
                pNodeInserted->m_pParent->m_pParent->m_color = NodeColor::k_red;
                pNodeInserted = pNodeInserted->m_pParent->m_pParent;
            }

            else  // uncle is black
            {
                // Case 2: Node's uncle is black and node is a right child.
                if (pNodeInserted == pNodeInserted->m_pParent->m_pRight)
                {
                    pNodeInserted = pNodeInserted->m_pParent;
                    RotateLeft(pNodeInserted);
                }

                // Case 3: Node's uncle is black and node is a left child.
                // Note that case 2 falls into case 3.
                pNodeInserted->m_pParent->m_color = NodeColor::k_black;
                pNodeInserted->m_pParent->m_pParent->m_color = NodeColor::k_red;
                RotateRight(pNodeInserted->m_pParent->m_pParent);
            }
        }

        else  // same as above with left & right exchanged
        {
            // get node's uncle
            Node* pUncle = pNodeInserted->m_pParent->m_pParent->m_pLeft;

            // Case 1: Node's uncle is red
            if (GetNodeColor(pUncle) == NodeColor::k_red)
            {
                pNodeInserted->m_pParent->m_color = NodeColor::k_black;
                pUncle->m_color = NodeColor::k_black;
                pNodeInserted->m_pParent->m_pParent->m_color = NodeColor::k_red;
                pNodeInserted = pNodeInserted->m_pParent->m_pParent;
            }

            else
            {
                // Case 2: Node's uncle is black and node is a right child
                if (pNodeInserted == pNodeInserted->m_pParent->m_pLeft)
                {
                    pNodeInserted = pNodeInserted->m_pParent;
                    RotateRight(pNodeInserted);
                }

                // Case 3: Node's uncle is black and node is a left child.
                // Note that case 2 falls into case 3.
                pNodeInserted->m_pParent->m_color = NodeColor::k_black;
                pNodeInserted->m_pParent->m_pParent->m_color = NodeColor::k_red;
                RotateLeft(pNodeInserted->m_pParent->m_pParent);
            }
        }
    }

    // root is always black
    m_pRoot->m_color = NodeColor::k_black;
}

    // Node rotation.  In this example, pNode == x.
    // 
    //     y  <-- RL  x
    //    /   RR -->   \
    //   x              y

    // Before Left Rotation:
    //       15
    //         \
    //          20
    //          /
    //         17

    // After Left Rotation:
    //       20
    //      /
    //     15
    //      \
    //       17

template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::RotateLeft(Node* pNode)
{
    // these nodes must be valid
    assert(pNode);
    assert(pNode->m_pRight);

    // Set the other node.  This is 'y' in the algorthm definition.
    Node* pOther = pNode->m_pRight;

    // turn the other node's left subtree into our node's right subtree.
    pNode->m_pRight = pOther->m_pLeft;
    if (pOther->m_pLeft)
        pOther->m_pLeft->m_pParent = pNode;

    // link our node's parent to the other node's parent
    pOther->m_pParent = pNode->m_pParent;

    if (pNode->m_pParent == nullptr)  // check to see if this is the root node
        m_pRoot = pOther;
    else if (pNode == pNode->m_pParent->m_pLeft)
        pNode->m_pParent->m_pLeft = pOther;
    else
        pNode->m_pParent->m_pRight = pOther;

    // put our node on the other's left node
    pOther->m_pLeft = pNode;
    pNode->m_pParent = pOther;
}

template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::RotateRight(Node* pNode)
{
    // these nodes must be valid
    assert(pNode);
    assert(pNode->m_pLeft);

    // Set the other node.  This is 'y' in the algorthm definition.
    Node* pOther = pNode->m_pLeft;

    // turn the other node's right subtree into our node's left subtree.
    pNode->m_pLeft = pOther->m_pRight;
    if (pOther->m_pRight)
        pOther->m_pRight->m_pParent = pNode;

    // link our node's parent to the other node's parent
    pOther->m_pParent = pNode->m_pParent;

    if (pNode->m_pParent == nullptr)  // check to see if this is the root node
        m_pRoot = pOther;
    else if (pNode == pNode->m_pParent->m_pRight)
        pNode->m_pParent->m_pRight = pOther;
    else
        pNode->m_pParent->m_pLeft = pOther;

    // put our node on the other's right node
    pOther->m_pRight = pNode;
    pNode->m_pParent = pOther;
}

template <class KeyType, class DataType>
typename BinarySearchTree<KeyType, DataType>::NodeColor BinarySearchTree<KeyType, DataType>::GetNodeColor(Node* pNode) const
{
    if (pNode == nullptr)
        return NodeColor::k_black;
    return pNode->m_color;
}


//---------------------------------------------------------------------------------------------------------------------
// Unit Test Helpers
//---------------------------------------------------------------------------------------------------------------------
template <class KeyType, class DataType>
void FindDataHelper(const BinarySearchTree<KeyType, DataType>& bst, KeyType key, bool shouldBeFound)
{
    DataType data;
    bool found = bst.FindData(key, data);

    if (found && shouldBeFound)
        cout << key << " Found: " << data << "\n";
    else if (found && !shouldBeFound)
        cout << "***FAIL*** " << key << " Found: " << data << "\n";
    else if (!found && shouldBeFound)
        cout << "***FAIL*** " << key << " Not found" << "\n";
    else  // !found && !shouldBeFound
        cout << key << " Not Found: " << data << "\n";
}


//---------------------------------------------------------------------------------------------------------------------
// Main
//---------------------------------------------------------------------------------------------------------------------
void main()
{
    BinarySearchTree<int, std::string> bst;

    // Test Insert()
    bst.Insert(11, "Eleven");
    bst.Insert(2, "Two");
    bst.Insert(14, "Fourteen");
    bst.Insert(1, "One");
    bst.Insert(7, "Seven");
    bst.Insert(15, "Fifteen");
    bst.Insert(8, "Eight");
    bst.Insert(5, "Five");

    //bst.PrintNodesInOrder();
    bst.PrintTree();
    cout << "\n\n";



    bst.Insert(4, "Four");

    //bst.PrintNodesInOrder();
    bst.PrintTree();

	std::cout << "__DELETING_THINGS__\n";

	bst.DeleteRBNode(2);
	//bst.DeleteRBNode(7);
	bst.DeleteRBNode(15);

	bst.PrintTree();

    _getch();
}

