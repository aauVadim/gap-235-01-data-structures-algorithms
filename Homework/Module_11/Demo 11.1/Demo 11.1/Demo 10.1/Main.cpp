// Main.cpp
#include <iostream>
#include <conio.h>
#include <assert.h>
#include <string>

using namespace std;

//---------------------------------------------------------------------------------------------------------------------
// Binary Search Tree Declaration
//---------------------------------------------------------------------------------------------------------------------
template <class KeyType, class DataType>
class BinarySearchTree
{
//public:
//    typedef _KeyType KeyType;
//    typedef _DataType DataType;

private:
    struct Node
    {
        Node* m_pParent;
        Node* m_pLeft;
        Node* m_pRight;

        KeyType m_key;
        DataType m_data;

        Node(const KeyType& key, const DataType& data)
            : m_pLeft(nullptr)
            , m_pRight(nullptr)
            , m_pParent(nullptr)
            , m_key(key)
            , m_data(data)
        {
            //
        }

        ~Node()
        {
            delete m_pLeft;
            delete m_pRight;

            m_pParent = nullptr;
            m_pLeft = nullptr;
            m_pRight = nullptr;
        }

        // used so we can surgically remove a node
        void ClearPointers()
        {
            m_pParent = nullptr;
            m_pLeft = nullptr;
            m_pRight = nullptr;
        }
    };


    Node* m_pRoot;

public:
    BinarySearchTree();
    ~BinarySearchTree();

    void Insert(const KeyType& key, const DataType& data);
	//---------------------------------------------------------------------------------------------------------------------
	// Homework, Assigment 12.3-1
	//---------------------------------------------------------------------------------------------------------------------
	void RecursiveInsert(const KeyType& key, const DataType& data);
	void WalkInsert(Node* pNode, Node* pNodeParent, const KeyType& key, const DataType& data);

    void Delete(const KeyType& key);

    bool FindData(const KeyType& key, DataType& outData) const;
    bool FindMinimum(DataType& outData) const;
    bool FindMaximum(DataType& outData) const;

    void PrintNodesInOrder() const;

private:
    Node* FindNode(const KeyType& key) const;
    Node* FindMinimumNode(Node* pRoot) const;
    Node* FindMaximumNode(Node* pRoot) const;
    void InternalPrintNodesInOrder(Node* pNode) const;
    void Transplant(Node* pNodeToReplace, Node* pReplacingNode);
};


//---------------------------------------------------------------------------------------------------------------------
// Binary Search Tree Definition
//---------------------------------------------------------------------------------------------------------------------
template <class KeyType, class DataType>
BinarySearchTree<KeyType, DataType>::BinarySearchTree()
    : m_pRoot(nullptr)
{
    //
}

template <class KeyType, class DataType>
BinarySearchTree<KeyType, DataType>::~BinarySearchTree()
{
    delete m_pRoot;
}

template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::Insert(const KeyType& key, const DataType& data)
{
    // create the new node
    Node* pNewNode = new Node(key, data);

    Node* pParent = nullptr;  // parent of the current node
    Node* pCurrent = m_pRoot;  // the current node

    // walk through the tree to find the spot to insert this node
    while (pCurrent)
    {
        // set last node, aka parent node
        pParent = pCurrent;

        // mode to the next branch
        if (key <= pCurrent->m_key)
            pCurrent = pCurrent->m_pLeft;
        else
            pCurrent = pCurrent->m_pRight;
    }

    // Once we get here, pParent will be the root of where we need to insert the new node.  If it's nullptr, it
    // means that the tree is empty, so we put it at the root.
    if (pParent == nullptr)
    {
        m_pRoot = pNewNode;
    }
    else if (key <= pParent->m_key)
    {
        pParent->m_pLeft = pNewNode;
        pNewNode->m_pParent = pParent;
    }
    else
    {
        pParent->m_pRight = pNewNode;
        pNewNode->m_pParent = pParent;
    }
}

//---------------------------------------------------------------------------------------------------------------------
// Homework, Assigment 12.3-1
//---------------------------------------------------------------------------------------------------------------------
template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::RecursiveInsert(const KeyType& key, const DataType& data)
{
	WalkInsert(m_pRoot, nullptr, key, data);
}

template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::WalkInsert(Node* pNode, Node* pNodeParent, const KeyType& key, const DataType& data)
{ 
	if (pNode)
	{
		if (key > pNode->m_key)
			WalkInsert(pNode->m_pRight, pNode, key, data);
		else
			WalkInsert(pNode->m_pLeft, pNode, key, data);
	}
	else
	{
		pNode = new Node(key, data);
		pNode->m_pParent = pNodeParent;

		if (pNodeParent)
		{
			if (pNodeParent->m_key > pNode->m_key)
				pNodeParent->m_pLeft = pNode;
			else
				pNodeParent->m_pRight = pNode;
		}
		else
		{
			m_pRoot = pNode;
		}
	
	}
}
//---------------------------------------------------------------------------------------------------------------------
// Homework, Assigment 12.3-1 - End 
//---------------------------------------------------------------------------------------------------------------------

template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::Delete(const KeyType& key)
{
    // find the node that we want to delete
    Node* pNodeToDelete = FindNode(key);
    if (!pNodeToDelete)
        return;

    // Case 1: Node has no left children.  It's a simple matter of shifting the right node up.
    if (pNodeToDelete->m_pLeft == nullptr)
    {
        Transplant(pNodeToDelete, pNodeToDelete->m_pRight);
    }

    // Case 2: Node has no right children.  Same thing; shift left node up.
    else if (pNodeToDelete->m_pRight == nullptr)
    {
        Transplant(pNodeToDelete, pNodeToDelete->m_pLeft);
    }

    // Case 3: Both children are valid.  :(
    else
    {
        // The successor is guaranteed to be the node with the smallest value in our node's right branch.  We need to 
        // surgically remove the successor and replace our node with it.
        Node* pSuccessor = FindMinimumNode(pNodeToDelete->m_pRight);
        assert(pSuccessor);  // this should be impossible

        // If the successor is not the direct child of the node we want to delete, we need to replace this successor 
        // with its right child.
        if (pSuccessor->m_pParent != pNodeToDelete)
        {
            Transplant(pSuccessor, pSuccessor->m_pRight);
            pSuccessor->m_pRight = pNodeToDelete->m_pRight;
            pSuccessor->m_pRight->m_pParent = pSuccessor;
        }

        Transplant(pNodeToDelete, pSuccessor);
        pSuccessor->m_pLeft = pNodeToDelete->m_pLeft;
        pSuccessor->m_pLeft->m_pParent = pSuccessor;
    }

    // destroy node
    pNodeToDelete->ClearPointers();
    delete pNodeToDelete;
}

template <class KeyType, class DataType>
bool BinarySearchTree<KeyType, DataType>::FindData(const KeyType& key, DataType& outData) const
{
    Node* pNode = FindNode(key);
    if (pNode)
    {
        outData = pNode->m_data;
        return true;
    }
    return false;
}

template <class KeyType, class DataType>
bool BinarySearchTree<KeyType, DataType>::FindMinimum(DataType& outData) const
{
    Node* pMinNode = FindMinimumNode(m_pRoot);
    if (pMinNode)
    {
        outData = pMinNode->m_data;
        return true;
    }

    return false;
}

template <class KeyType, class DataType>
bool BinarySearchTree<KeyType, DataType>::FindMaximum(DataType& outData) const
{
    Node* pMaxNode = FindMaximumNode(m_pRoot);
    if (pMaxNode)
    {
        outData = pMaxNode->m_data;
        return true;
    }

    return false;
}

template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::PrintNodesInOrder() const
{
    InternalPrintNodesInOrder(m_pRoot);
}

template <class KeyType, class DataType>
typename BinarySearchTree<KeyType, DataType>::Node* BinarySearchTree<KeyType, DataType>::FindNode(const KeyType& key) const
{
    Node* pCurrent = m_pRoot;
    while (pCurrent)
    {
        // if this is the node we're looking for, return it
        if (key == pCurrent->m_key)
            return pCurrent;

        // move to the next branch
        if (key <= pCurrent->m_key)
            pCurrent = pCurrent->m_pLeft;
        else
            pCurrent = pCurrent->m_pRight;
    }

    // if we get here, we didn't find the node
    return nullptr;
}

template <class KeyType, class DataType>
typename BinarySearchTree<KeyType, DataType>::Node* BinarySearchTree<KeyType, DataType>::FindMinimumNode(Node* pRoot) const
{
    if (!pRoot)
        return nullptr;

    Node* pCurrent = pRoot;
    while (pCurrent->m_pLeft)
    {
        pCurrent = pCurrent->m_pLeft;
    }

    return pCurrent;
}

template <class KeyType, class DataType>
typename BinarySearchTree<KeyType, DataType>::Node* BinarySearchTree<KeyType, DataType>::FindMaximumNode(Node* pRoot) const
{
    if (!pRoot)
        return nullptr;

    Node* pCurrent = pRoot;
    while (pCurrent->m_pRight)
    {
        pCurrent = pCurrent->m_pRight;
    }

    return pCurrent;
}

template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::InternalPrintNodesInOrder(Node* pNode) const
{
    if (pNode)
    {
        InternalPrintNodesInOrder(pNode->m_pLeft);
        std::cout << pNode->m_key << " => " << pNode->m_data << "\n";
        InternalPrintNodesInOrder(pNode->m_pRight);
    }
}

template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::Transplant(Node* pNodeToReplace, Node* pReplacingNode)
{
    // check to see if we're replacing the root node
    if (!pNodeToReplace->m_pParent)
        m_pRoot = pReplacingNode;

    // If we get here, we're either a right or a left child node.  Handle each case by pointing those nodes to 
    // the replacing node.
    else if (pNodeToReplace == pNodeToReplace->m_pParent->m_pLeft)
        pNodeToReplace->m_pParent->m_pLeft = pReplacingNode;
    else
        pNodeToReplace->m_pParent->m_pRight = pReplacingNode;

    // update the parent of the replacing node, assuming the replacing node is valid
    if (pReplacingNode)
        pReplacingNode->m_pParent = pNodeToReplace->m_pParent;
}


//---------------------------------------------------------------------------------------------------------------------
// Unit Test Helpers
//---------------------------------------------------------------------------------------------------------------------
template <class KeyType, class DataType>
void FindDataHelper(const BinarySearchTree<KeyType, DataType>& bst, KeyType key, bool shouldBeFound)
{
    DataType data;
    bool found = bst.FindData(key, data);

    if (found && shouldBeFound)
        cout << key << " Found: " << data << "\n";
    else if (found && !shouldBeFound)
        cout << "***FAIL*** " << key << " Found: " << data << "\n";
    else if (!found && shouldBeFound)
        cout << "***FAIL*** " << key << " Not found" << "\n";
    else  // !found && !shouldBeFound
        cout << key << " Not Found: " << data << "\n";
}


//---------------------------------------------------------------------------------------------------------------------
// Main
//---------------------------------------------------------------------------------------------------------------------
void main()
{
    BinarySearchTree<int, std::string> bst;

    // Test Insert()
    bst.Insert(5, "Five");
    bst.Insert(2, "Two");
    bst.Insert(3, "Three");
    bst.Insert(1, "One");
    bst.Insert(7, "Seven");
    bst.Insert(10, "Ten");
    bst.Insert(8, "Eight");

    bst.PrintNodesInOrder();

    // Find()
    cout << "\n\n";
    FindDataHelper(bst, 7, true);
    FindDataHelper(bst, 10, true);
    FindDataHelper(bst, 2, true);
    FindDataHelper(bst, 17, false);
    FindDataHelper(bst, 110, false);
    FindDataHelper(bst, -4, false);

    // delete
    cout << "\n\n";

    bst.Insert(20, "Twenty");
    bst.Insert(15, "Fifteen");
    bst.Insert(25, "TwentyFive");
    bst.Insert(12, "Twelve");
    bst.Insert(17, "Seventeen");

    bst.Delete(10);  // this is the hardest case

    bst.Delete(1);
    bst.Delete(2);
    bst.Delete(5);

    bst.PrintNodesInOrder();

	std::cout << "Insert nodes Recurcive" << std::endl;
	bst.RecursiveInsert(13, "Thirtien");
	bst.RecursiveInsert(9, "Nine");


	bst.PrintNodesInOrder();


    _getch();
}

