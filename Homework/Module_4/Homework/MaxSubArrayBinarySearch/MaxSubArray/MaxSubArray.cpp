// MaxSubArray.cpp

// Modified:
// Name: Vadim Osipov
// Date: 10/5

// [vo]: Rez - I used your MaxSubArrayCrossing code here. I figured that the point of this assignment was to understand how it works. 
// hope I've done that right. Thank you. 

#include <iostream>
#include <conio.h>

using std::cout;
using std::endl;

const int k_stockPriceDeltas = 16;
const int k_infinity = 0x7ffffff; //Infinity

struct MaxSubArrayReturn
{
	int m_low, m_high, m_desiredNumber;

	MaxSubArrayReturn(int low, int high, int desiredNumber) 
		: m_low(low)
		, m_high(high)
		, m_desiredNumber(desiredNumber)
	{ }

	MaxSubArrayReturn& operator=(const MaxSubArrayReturn& right)
	{
		m_low = right.m_low;
		m_high = right.m_high;
		m_desiredNumber = right.m_desiredNumber;
	}
};

MaxSubArrayReturn FindMaximumSubArray(int* pArray, int low, int high, int desiredNumber);
MaxSubArrayReturn FindNumberSubArray(int* pArray, int low, int mid, int high, int desiredNumber);

void PrintArray(int* pArray, int size);

void main()
{
	int change[k_stockPriceDeltas] = { 13, -3, -25, 20, -3, -16, -23, 18, 20, -7, 12, -5, -22, 15, -4, 7 };

	int searchNumber = 0;
	PrintArray(change, k_stockPriceDeltas);

	std::cout << "Endter desired number: ";
	std::cin >> searchNumber;
	std::cout << std::endl;

	MaxSubArrayReturn ret = FindMaximumSubArray(change, 0, k_stockPriceDeltas, searchNumber);

	if (ret.m_desiredNumber == searchNumber)
	{
		cout << "Subarray With Number: " << ret.m_low << " - " << ret.m_high << " Desired value: " << ret.m_desiredNumber << endl;
		_getch();
	}
	else
	{
		cout << "Did not find such number"<< endl;
		_getch();
	}
}

void PrintArray(int* pArray, int size)
{
	using namespace std;

	cout << "Array: {"; 
	for (int i = 0; i < size; ++i)
	{
		cout << pArray[i] << ", ";
	}
	cout << "} " << endl;
}

MaxSubArrayReturn FindMaximumSubArray(int* pArray, int low, int high, int desiredNumber)
{
	// only one element
	if (low == high - 1)
		return MaxSubArrayReturn(low, high, pArray[low]);

	int mid = (low + high) / 2;

	MaxSubArrayReturn leftReturn = FindMaximumSubArray(pArray, low, mid, desiredNumber);
	MaxSubArrayReturn rightReturn = FindMaximumSubArray(pArray, mid, high, desiredNumber);
	MaxSubArrayReturn crossReturn = FindNumberSubArray(pArray, low, mid, high, desiredNumber);
	
	//
	if (leftReturn.m_desiredNumber == desiredNumber)
		return leftReturn;
	if (rightReturn.m_desiredNumber == desiredNumber)
		return rightReturn;

	//So function returns something
	return crossReturn;
}

MaxSubArrayReturn FindNumberSubArray(int* pArray, int low, int mid, int high, int desiredNumber)
{

	// start at the left
	int maxLeft = -1;
	
	int searchNumber = -1;

	// loop from mid to low
	for (int i = mid - 1; i >= low; --i)
	{
		if (pArray[i] == desiredNumber)
			searchNumber = pArray[i];
	}

	//then right
	int maxRight = -1;

	// mid to high
	for (int i = mid; i < high; ++i)
	{
		if (pArray[i] == desiredNumber)
			searchNumber = pArray[i];
	}

	return MaxSubArrayReturn(maxLeft, maxRight, searchNumber);
}
