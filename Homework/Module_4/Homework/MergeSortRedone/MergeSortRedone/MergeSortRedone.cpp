// MergeSortRedone.cpp
// Name: Vadim Osipov
// Date: 9/30

#include <iostream>
#include <conio.h>

using std::cout;
using std::endl;

void MergeSort(int* pArray, int start, int end);
void Merge(int* pArray, int start, int middle, int end);
void PrintArray(int* pArray, int size);

const int k_arraySize = 12;
const int k_infinity = 0x7ffffff;

const int k_arrayHalf = (k_arraySize / 2) + 1;

void main()
{
	int intArray[k_arraySize] =
	{
		1, 3, 5, 9, 15,
		2, 4, 6, 8, 10, 12, 20
	};

	int anotherArray[k_arraySize] = { 21, 54, 0, 32, 12, 1, -4, 7, 89, 19, 11, 35 };

	cout << "Pre-Sort: ";
	PrintArray(anotherArray, k_arraySize);

	MergeSort(anotherArray, 0, k_arraySize);

	cout << "Post-Sort: ";
	PrintArray(anotherArray, k_arraySize);

	_getch();
}

void MergeSort(int* pArray, int start, int end)
{
	if (start < end - 1)
	{
		int middle = (start + end) / 2;
		MergeSort(pArray, start, middle);
		MergeSort(pArray, middle, end);
		Merge(pArray, start, middle, end);
	}
}

void Merge(int* pArray, int start, int middle, int end)
{
	// initialize the left array
	int leftSize = middle - start + 1;  // + 1 because of terminator

	//Initializing left side array
	int leftArray[k_arrayHalf] = { 0 };

	// initialize the right array
	int rightSize = end - middle + 1;  // + 1 because of terminator

	//Initializing right side array
	int rightArray[k_arrayHalf] = { 0 };

	// fill the left array
	for (int i = 0; i < leftSize; ++i)
	{
		leftArray[i] = pArray[start + i];
	}
	leftArray[leftSize - 1] = k_infinity;

	// fill the right array
	for (int i = 0; i < rightSize; ++i)
	{
		rightArray[i] = pArray[middle + i];
	}
	rightArray[rightSize - 1] = k_infinity;

	int leftIndex = 0;
	int rightIndex = 0;

	// merge
	for (int i = start; i < end; ++i)
	{
		if (leftArray[leftIndex] <= rightArray[rightIndex])
		{
			pArray[i] = leftArray[leftIndex];
			++leftIndex;
		}
		else
		{
			pArray[i] = rightArray[rightIndex];
			++rightIndex;
		}
	}
}

void PrintArray(int* pArray, int size)
{
	cout << "{ ";
	for (int i = 0; i < size; ++i)
	{
		cout << pArray[i] << ", ";
	}
	cout << " }\n";
}



