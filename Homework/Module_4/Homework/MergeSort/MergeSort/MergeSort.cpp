// MergeSort.cpp
#include <iostream>
#include <conio.h>

using std::cout;

void MergeSort(int* pArray, int start, int end);
void Merge(int* pArray, int start, int middle, int end);
void PrintArray(int* pArray, int size);

const int k_arraySize = 12;
const int k_infinity = 0x7ffffff;

void main()
{
	int intArray[k_arraySize] =
	{
		1, 3, 5, 9, 15,
		2, 4, 6, 8, 10, 12, 20
	};

	int anotherArray[k_arraySize] = { 21, 54, 0, 32, 12, 1, -4, 7, 89, 19, 11, 35 };

	cout << "Pre-Sort: ";
	//PrintArray(intArray, k_arraySize);
	PrintArray(anotherArray, k_arraySize);

	MergeSort(anotherArray, 0, k_arraySize);
	//Merge(intArray, 0, 5, k_arraySize);

	cout << "Post-Sort: ";
	//PrintArray(intArray, k_arraySize);
	PrintArray(anotherArray, k_arraySize);

	_getch();
}

void MergeSort(int* pArray, int start, int end)
{
	if (start < end - 1)
	{
		int middle = (start + end) / 2;
		MergeSort(pArray, start, middle);
		MergeSort(pArray, middle, end);
		Merge(pArray, start, middle, end);
	}
}

void Merge(int* pArray, int start, int middle, int end)
{
	// initialize the left array
	int leftSize = middle - start + 1;  // + 1 because of terminator
	int* pLeft = new int[leftSize];
	memset(pLeft, 0, sizeof(int) * leftSize);

	// initialize the right array
	int rightSize = end - middle + 1;  // + 1 because of terminator
	int* pRight = new int[rightSize];
	memset(pRight, 0, sizeof(int) * rightSize);

	// fill the left array
	for (int i = 0; i < leftSize; ++i)
	{
		pLeft[i] = pArray[start + i];
	}
	pLeft[leftSize - 1] = k_infinity;

	// fill the right array
	for (int i = 0; i < rightSize; ++i)
	{
		pRight[i] = pArray[middle + i];
	}
	pRight[rightSize - 1] = k_infinity;

	int leftIndex = 0;
	int rightIndex = 0;

	// merge
	for (int i = start; i < end; ++i)
	{
		if (pLeft[leftIndex] <= pRight[rightIndex])
		{
			pArray[i] = pLeft[leftIndex];
			++leftIndex;
		}
		else
		{
			pArray[i] = pRight[rightIndex];
			++rightIndex;
		}
	}

	delete[] pLeft;
	delete[] pRight;
}

void PrintArray(int* pArray, int size)
{
	cout << "{ ";
	for (int i = 0; i < size; ++i)
	{
		cout << pArray[i] << ", ";
	}
	cout << " }\n";
}



