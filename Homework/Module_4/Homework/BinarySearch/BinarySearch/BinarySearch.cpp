// BinarySearch.cpp
// Name: Vadim Osipov 
// Date: 10/9

#include <iostream>
#include <conio.h>

const int k_arraySize = 6;
const int k_infinity = 0x7ffffff;

int BinarySearch(int* pArray, int start, int end, int desiredNumber);
void SearchArray(int* pArray, int start, int middle, int end);
void PrintArray(int* pArray, int size);

void main()
{
	int intArray[k_arraySize] = {1, 4, 7, 12, 16, 21};
	
	PrintArray(intArray, k_arraySize);

	int desiredNumber; 
	std::cout << "Please enter desired number: \n";
	std::cin >> desiredNumber;

	int numberFound = BinarySearch(intArray, 0, k_arraySize, desiredNumber);
}

int BinarySearch(int* pArray, int start, int end, int desiredNumber)
{
	int key = -1; 

	if (start < end - 1)
	{
		//Getting the middle
		int middle = (start + end) / 2;

		//If we found a number in the middle
		if (pArray[middle] == desiredNumber)
			return key = desiredNumber;

		//Number is located in left half 
		if (pArray[middle] > desiredNumber)
		{
			return key = BinarySearch(pArray, start, middle, desiredNumber);
		}
		//Number located in right half
		else if (pArray[middle] < desiredNumber)
		{
			return key = BinarySearch(pArray, middle, end, desiredNumber); 
		}
	}
	return key;
}

void PrintArray(int* pArray, int size)
{

	std::cout << "{ ";
	for (int i = 0; i < size; ++i)
	{
		std::cout << pArray[i] << ", ";
	}
	std::cout << " }\n";
}




