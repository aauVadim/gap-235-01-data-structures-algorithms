// MaxSubArray.cpp
#include <iostream>
#include <conio.h>

using std::cout;
using std::endl;

const int k_stockPriceDeltas = 16;
const int k_infinity = 0x7ffffff; //Infinity

struct MaxSubArrayReturn
{
	int m_low, m_high, m_sum;

	MaxSubArrayReturn(int low, int high, int sum) 
		: m_low(low)
		, m_high(high)
		, m_sum(sum)
	{ }

	MaxSubArrayReturn& operator=(const MaxSubArrayReturn& right)
	{
		m_low = right.m_low;
		m_high = right.m_high;
		m_sum = right.m_sum;
	}
};

MaxSubArrayReturn FindMaximumSubArray(int* pArray, int low, int high);
MaxSubArrayReturn FindMaxCrossingSubArray(int* pArray, int low, int mid, int high);

void main()
{
	//int stockPrices[k_stockPriceDays] = { 100, 113, 110, 85, 105, 102, 86, 63, 81, 101, 94, 106, 101, 79, 94, 90, 97 };
	int change[k_stockPriceDeltas] = { 13, -3, -25, 20, -3, -16, -23, 18, 20, -7, 12, -5, -22, 15, -4, 7 };

	MaxSubArrayReturn ret = FindMaximumSubArray(change, 0, k_stockPriceDeltas);

	cout << "Max Subarray: " << ret.m_low << " - " << ret.m_high << " with a value of " << ret.m_sum << endl;
	_getch();
}

MaxSubArrayReturn FindMaximumSubArray(int* pArray, int low, int high)
{
	// only one element
	if (low == high - 1)
		return MaxSubArrayReturn(low, high, pArray[low]);

	int mid = (low + high) / 2;

	MaxSubArrayReturn leftReturn = FindMaximumSubArray(pArray, low, mid);
	MaxSubArrayReturn rightReturn = FindMaximumSubArray(pArray, mid, high);
	MaxSubArrayReturn crossReturn = FindMaxCrossingSubArray(pArray, low, mid, high);

	if (leftReturn.m_sum >= rightReturn.m_sum && leftReturn.m_sum >= crossReturn.m_sum)
		return leftReturn;
	else if (rightReturn.m_sum >= leftReturn.m_sum && rightReturn.m_sum >= crossReturn.m_sum)
		return rightReturn;
	else
		return crossReturn;
}

MaxSubArrayReturn FindMaxCrossingSubArray(int* pArray, int low, int mid, int high)
{
	// start at the left
	int leftSum = -k_infinity;
	int sum = 0;
	int maxLeft = -1;

	// loop from mid to low
	for (int i = mid - 1; i >= low; --i)
	{
		sum += pArray[i];
		if (sum > leftSum)
		{
			leftSum = sum;
			maxLeft = i;
		}
	}

	// move on to the right
	int rightSum = -k_infinity;
	sum = 0;
	int maxRight = -1;

	// mid to high
	for (int i = mid; i < high; ++i)
	{
		sum += pArray[i];
		if (sum > rightSum)
		{
			rightSum = sum;
			maxRight = i;
		}
	}

	return MaxSubArrayReturn(maxLeft, maxRight, leftSum + rightSum);
}
