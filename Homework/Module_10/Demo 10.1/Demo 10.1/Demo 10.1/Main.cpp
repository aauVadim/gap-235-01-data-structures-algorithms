// Main.cpp
#include <iostream>
#include <conio.h>
#include <assert.h>
#include <string>

using namespace std;

//---------------------------------------------------------------------------------------------------------------------
// Binary Search Tree Declaration
//---------------------------------------------------------------------------------------------------------------------
template <class KeyType, class DataType>
class BinarySearchTree
{
//public:
//    typedef _KeyType KeyType;
//    typedef _DataType DataType;

private:
    struct Node
    {
        Node* m_pParent;
        Node* m_pLeft;
        Node* m_pRight;

        KeyType m_key;
        DataType m_data;

        Node(const KeyType& key, const DataType& data)
            : m_pLeft(nullptr)
            , m_pRight(nullptr)
            , m_pParent(nullptr)
            , m_key(key)
            , m_data(data)
        {
            //
        }

        ~Node()
        {
            delete m_pLeft;
            delete m_pRight;

            m_pParent = nullptr;
            m_pLeft = nullptr;
            m_pRight = nullptr;
        }
    };


    Node* m_pRoot;

public:
    BinarySearchTree();
    ~BinarySearchTree();

    void Insert(const KeyType& key, const DataType& data);

    bool FindData(const KeyType& key, DataType& outData) const;
    bool FindMinimum(DataType& outData) const;
    bool FindMaximum(DataType& outData) const;

    void PrintNodesInOrder() const;

private:
    Node* FindNode(const KeyType& key) const;
    Node* FindMinimumNode(Node* pRoot) const;
    Node* FindMaximumNode(Node* pRoot) const;
    void InternalPrintNodesInOrder(Node* pNode) const;
	//Homework
	void InternalPrintNodesInOrderNonRecurcive(Node* pNode) const; 
	void InternalRecursiveMinimum(const Node* pNode) const;
	void InternalRecursiveMaximum(const Node* pNode) const;
};


//---------------------------------------------------------------------------------------------------------------------
// Binary Search Tree Definition
//---------------------------------------------------------------------------------------------------------------------
template <class KeyType, class DataType>
BinarySearchTree<KeyType, DataType>::BinarySearchTree()
    : m_pRoot(nullptr)
{
    //
}

template <class KeyType, class DataType>
BinarySearchTree<KeyType, DataType>::~BinarySearchTree()
{
    delete m_pRoot;
}

template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::Insert(const KeyType& key, const DataType& data)
{
    // create the new node
    Node* pNewNode = new Node(key, data);

    Node* pParent = nullptr;  // parent of the current node
    Node* pCurrent = m_pRoot;  // the current node

    // walk through the tree to find the spot to insert this node
    while (pCurrent)
    {
        // set last node, aka parent node
        pParent = pCurrent;

        // mode to the next branch
        if (key <= pCurrent->m_key)
            pCurrent = pCurrent->m_pLeft;
        else
            pCurrent = pCurrent->m_pRight;
    }

    // Once we get here, pParent will be the root of where we need to insert the new node.  If it's nullptr, it
    // means that the tree is empty, so we put it at the root.
    if (pParent == nullptr)
    {
        m_pRoot = pNewNode;
    }
    else if (key <= pParent->m_key)
    {
        pParent->m_pLeft = pNewNode;
        pNewNode->m_pParent = pParent;
    }
    else
    {
        pParent->m_pRight = pNewNode;
        pNewNode->m_pParent = pParent;
    }
}

template <class KeyType, class DataType>
bool BinarySearchTree<KeyType, DataType>::FindData(const KeyType& key, DataType& outData) const
{
    Node* pNode = FindNode(key);
    if (pNode)
    {
        outData = pNode->m_data;
        return true;
    }
    return false;
}

template <class KeyType, class DataType>
bool BinarySearchTree<KeyType, DataType>::FindMinimum(DataType& outData) const
{
    Node* pMinNode = FindMinimumNode(m_pRoot);
    if (pMinNode)
    {
        outData = pMinNode->m_data;
        return true;
    }

    return false;
}

template <class KeyType, class DataType>
bool BinarySearchTree<KeyType, DataType>::FindMaximum(DataType& outData) const
{
    Node* pMaxNode = FindMaximumNode(m_pRoot);
    if (pMaxNode)
    {
        outData = pMaxNode->m_data;
        return true;
    }

    return false;
}
//I used this function to run all of the tests
template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::PrintNodesInOrder() const
{
	std::cout << "Recursive\n";
	//Printing Nodes in recurcive way
    InternalPrintNodesInOrder(m_pRoot);

	std::cout << "Non Recurcive\n";
	//Printing Nodes in non recurcive way
	InternalPrintNodesInOrderNonRecurcive(m_pRoot);

	std::cout << "Min/Max\n";
	InternalRecursiveMinimum(m_pRoot);
	InternalRecursiveMaximum(m_pRoot);
}

template <class KeyType, class DataType>
typename BinarySearchTree<KeyType, DataType>::Node* BinarySearchTree<KeyType, DataType>::FindNode(const KeyType& key) const
{
    Node* pCurrent = m_pRoot;
    while (pCurrent)
    {
        // if this is the node we're looking for, return it
        if (key == pCurrent->m_key)
            return pCurrent;

        // move to the next branch
        if (key <= pCurrent->m_key)
            pCurrent = pCurrent->m_pLeft;
        else
            pCurrent = pCurrent->m_pRight;
    }

    // if we get here, we didn't find the node
    return nullptr;
}

template <class KeyType, class DataType>
typename BinarySearchTree<KeyType, DataType>::Node* BinarySearchTree<KeyType, DataType>::FindMinimumNode(Node* pRoot) const
{
    if (!pRoot)
        return nullptr;

    Node* pCurrent = pRoot;
    while (pCurrent->m_pLeft)
    {
        pCurrent = pCurrent->m_pLeft;
    }

    return pCurrent;
}

template <class KeyType, class DataType>
typename BinarySearchTree<KeyType, DataType>::Node* BinarySearchTree<KeyType, DataType>::FindMaximumNode(Node* pRoot) const
{
    if (!pRoot)
        return nullptr;

    Node* pCurrent = pRoot;
    while (pCurrent->m_pRight)
    {
        pCurrent = pCurrent->m_pRight;
    }

    return pCurrent;
}

template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::InternalPrintNodesInOrder(Node* pNode) const
{
    if (pNode)
    {
        InternalPrintNodesInOrder(pNode->m_pLeft);
        std::cout << pNode->m_key << " => " << pNode->m_data << "\n";
        InternalPrintNodesInOrder(pNode->m_pRight);
    }
}


//---------------------------------------------------------------------------------------------------------------------
//	12.1-3 Exercise. Fully assuming that we have a balansed three
//	Broken, doest walk the three in order. 
//---------------------------------------------------------------------------------------------------------------------
template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::InternalPrintNodesInOrderNonRecurcive(Node* pNode) const
{
	//Getting the minimum number
	Node* pCurrent = FindMinimumNode(m_pRoot);
	Node* pNext = pCurrent;

	while (pCurrent)
	{
		std::cout << pNext->m_key << " => " << pNext->m_data << "\n";
		
		//We are on the left
		if (pCurrent->m_key < m_pRoot->m_key)
		{
			if (pCurrent->m_pParent && pCurrent->m_pParent->m_pRight)
			{
				if (pNext != pCurrent->m_pParent)
				{
					pNext = pCurrent->m_pParent;
				}
				else if (pNext != pCurrent->m_pParent->m_pRight)
				{
					pNext = pCurrent->m_pParent->m_pRight;
					pCurrent = pCurrent->m_pParent;
				}
				else
				{
					pNext = pCurrent->m_pParent->m_pParent;
				}
			}
		}
		//We are on the right
		else
		{
			if (pNext->m_pLeft)
			{
				pNext = pNext->m_pLeft;
			}
			else if (pNext->m_pRight)
			{
				pCurrent = pNext;
				pNext = pNext->m_pRight;
			}
			else
			{
				pCurrent = nullptr;
			}
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------
//	12.2-2 Exercise
//---------------------------------------------------------------------------------------------------------------------
template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::InternalRecursiveMinimum(const Node* pNode) const
{
	if (pNode)
	{
		if (pNode->m_pLeft)
			InternalRecursiveMinimum(pNode->m_pLeft);
		else
			std::cout << pNode->m_key << " => " << pNode->m_data << "\n";
	}
}
//---------------------------------------------------------------------------------------------------------------------
//	12.2-2 Exercise.
//---------------------------------------------------------------------------------------------------------------------
template <class KeyType, class DataType>
void BinarySearchTree<KeyType, DataType>::InternalRecursiveMaximum(const Node* pNode) const
{
	if (pNode)
	{
		if (pNode->m_pRight)
			InternalRecursiveMaximum(pNode->m_pRight);
		else
			std::cout << pNode->m_key << " => " << pNode->m_data << "\n";
	}
}


//---------------------------------------------------------------------------------------------------------------------
// Unit Test Helpers
//---------------------------------------------------------------------------------------------------------------------
template <class KeyType, class DataType>
void FindDataHelper(const BinarySearchTree<KeyType, DataType>& bst, KeyType key, bool shouldBeFound)
{
    DataType data;
    bool found = bst.FindData(key, data);

    if (found && shouldBeFound)
        cout << key << " Found: " << data << "\n";
    else if (found && !shouldBeFound)
        cout << "***FAIL*** " << key << " Found: " << data << "\n";
    else if (!found && shouldBeFound)
        cout << "***FAIL*** " << key << " Not found" << "\n";
    else  // !found && !shouldBeFound
        cout << key << " Not Found: " << data << "\n";
}


//---------------------------------------------------------------------------------------------------------------------
// Main
//---------------------------------------------------------------------------------------------------------------------
void main()
{
    BinarySearchTree<int, std::string> bst;

    // Test Insert()
    bst.Insert(5, "Five");
    bst.Insert(2, "Two");
    bst.Insert(3, "Three");
    bst.Insert(1, "One");
    bst.Insert(7, "Seven");
    bst.Insert(10, "Ten");
    bst.Insert(8, "Eight");

    bst.PrintNodesInOrder();

    // Find()
    cout << "\n\n";
    FindDataHelper(bst, 7, true);
    FindDataHelper(bst, 10, true);
    FindDataHelper(bst, 2, true);
    FindDataHelper(bst, 17, false);
    FindDataHelper(bst, 110, false);
    FindDataHelper(bst, -4, false);

    _getch();
}

