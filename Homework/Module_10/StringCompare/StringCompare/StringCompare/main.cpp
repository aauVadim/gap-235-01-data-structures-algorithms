// main.cpp
#include <iostream>
#include <conio.h>

bool CompareStrings(const char* pLeftString, const char* pRightString)
{
    // What if they're both null?
    if (pLeftString == nullptr && pRightString == nullptr)
        return true;

    // One is null but the other isn't, so they can't be equivilant.
    else if (pLeftString == nullptr || pRightString == nullptr)
        return false;

    // loop through the string
    int index = 0;
    while (pLeftString[index] != '\0' || pRightString[index] != '\0')
    {
        if (pLeftString[index] != pRightString[index])
            return false;
        ++index;
    }

    return pLeftString[index] == pRightString[index];
}

void main()
{
    if (CompareStrings("Cat", "Cat") == false)
        std::cout << "FAIL\n";
    if (CompareStrings("Cat", "CatCall") == true)
        std::cout << "FAIL\n";
    if (CompareStrings("", "") == false)
        std::cout << "FAIL\n";
    if (CompareStrings(nullptr, nullptr) == false)
        std::cout << "FAIL\n";
    if (CompareStrings("Cat", nullptr) == true)
        std::cout << "FAIL\n";
    if (CompareStrings("Cat", "Dog") == true)
        std::cout << "FAIL\n";

    std::cout << "Done!";

    _getch();
}
