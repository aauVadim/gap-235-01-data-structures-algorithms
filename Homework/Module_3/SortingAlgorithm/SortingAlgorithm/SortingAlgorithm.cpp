// SortingAlgorithm.h
// Name: Vadim Osipov 
// Date: 9/27

#include<iostream>
#include<conio.h>

void Sort(int inputArray[], int size);
void Print(int inputArray[], int size);

void main()
{
	int input[] = {10, 5, 62, 9, 23, 54};
	
	Print(input, 6);

	Sort(input, 6);

	Print(input, 6);

	_getch();
}

void Print(int inputArray[], int size)
{
	std::cout << "Array: {"; 
	for (int i = 0; i < size; ++i)
	{
		std::cout << " " << inputArray[i];
	}
	std::cout << " }\n";
}

void Sort(int inputArray[], int size)
{
	for (int i = 0; i < size; ++i)															//C1*n
	{
		int key = inputArray[i];	//Pick element											//C2
		int j = i + 1;				//Start from next item									//C3
		int smallNum = key;			//Assuming that we are looking at the small number		//C4
		while (j < size)			//Walk trough array										//C5*(n-1) - is that right? 
		{
			//Start at i compare to next 
			if (smallNum > inputArray[j])													//C6 - "gotta have some n" 
			{
				//Save smallest found 
				smallNum = inputArray[j];													//C6 - "gotta have some n"
				//Swap them places - so we do not loose i item
				inputArray[j] = key;														//C6 - "gotta have some n"
			}

			//Debuggin'
			//std::cout << "Current smallest: " << smallNum 
			//	<< " Step: i = " << i 
			//	<< " Next: j = "  << j 
			//	<< std::endl;
			//_getch();

			//Next step
			++j;																			//C*(n-1)
		}
		//Place the smallest we found into current spot
		inputArray[i] = smallNum;															//C
	}
}