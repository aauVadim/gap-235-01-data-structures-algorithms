// CountingSort.cpp
// Name: Vadim Osipov 
// Date: 10/12

#include <iostream>
#include <conio.h>

const int k_arraySize = 6; 
const int k_countArraySize = 6;


//[vo]TOREZ - For some reason it looses one number right now to 0; I cannot figure out why.

void CountingSort(int* pArray, int* pOutArray, const int size, const int biggest);
void PrintArray(int* pArray, const int size);

void main()
{
	int intArray[k_arraySize] = { 12, 4, 5, 2, 1, 5 };
	int outputArray[k_arraySize] = { 0 };

	PrintArray(intArray, k_arraySize);

	CountingSort(intArray, outputArray, k_arraySize, 12);

	std::cout << "Sorted: " << std::endl;
	PrintArray(intArray, k_arraySize);
	PrintArray(outputArray, k_arraySize);

	_getch();
}
//Trying to completely follow the Sudo code from the book p.195 
void CountingSort(int* pArray, int* pOutArray, const int size, const int biggest)
{
	//Making count array
	int* pNewArray = new int[biggest + 1];

	//Zeros 
	for (int i = 0; i <= biggest; ++i)
		pNewArray[i] = 0;

	//Count how many times we meet each number in array
	for (int j = 1; j < size; ++j)
	{
		pNewArray[pArray[j]] = pNewArray[pArray[j]] + 1;
	}
	//Summ up: i = i + (i - 1)
	for (int i = 1; i <= biggest; ++i)
	{
		pNewArray[i] = pNewArray[i] + pNewArray[i - 1];
	}
	//Loop trough array and assing values to outArray
	//[???] Loosing one number. Cannot find why 
	for (int j = k_arraySize -1; j >= 0; --j)
	{
		pOutArray[pNewArray[pArray[j]] - 1] = pArray[j];
		pNewArray[pArray[j]] = pNewArray[pArray[j]] - 1;
	}

}


void PrintArray(int* pArray, const int size)
{
	std::cout << "{ ";
	for (int i = 0; i < size; ++i)
	{
		std::cout << pArray[i] << ", ";
	}
	std::cout << " }\n";
}




